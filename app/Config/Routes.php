<?php

use App\Controllers\Laporan\Test;
use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get('/test/(:segment)/(:segment)', 'Home::index2/$1/$2');
$routes->get('/test', 'Home::index2');



$routes->get('/testhome', 'TestHome::index');



$routes->get('/new', 'TestHome::new',['as'=>'test.new']);

$routes->post('/new', 'TestHome::create',['as'=>'test.create']);

service('auth')->routes($routes);


$routes->group('admin',['filter'=>'group:user'], static function ($routes) {

    $routes->get('aduan', 'Aduan::index', ['as' => 'admin.aduan.index']);
    $routes->get('aduan_ajax', 'Aduan::ajaxDataAduan', ['as' => 'admin.aduan.ajaxDataAduan']);
    $routes->get('aduan/edit/(:segment)', 'Aduan::edit/$1', ['as' => 'admin.aduan.edit']);
    $routes->put('aduan/update/(:segment)', 'Aduan::update/$1',
     ['as' => 'admin.aduan.update']);
     $routes->delete('aduan/delete/(:segment)', 'Aduan::delete/$1', ['as' => 'admin.aduan.delete']);

     $routes->get('access/token', static function() {

        $token = auth()->user()->generateAccessToken(service('request')->getVar('token_name'));
    
        return json_encode(['token' => $token->raw_token]);
    });



    $routes->get('aduanapi', 'Aduan::callApiBantuan', ['as' => 'admin.aduan.api']);
    $routes->get('aduanapi2', 'Aduan::callApiBantuan2', ['as' => 'admin.aduan.api2']);
    
});



$routes->resource('api/aduan',['controller' => 'Api\ApiAduan' ,'filter' => 'tokens']);




$routes->get('/laporan', 'Laporan\Test::index');
// $routes->get('/apiaduan', 'Api\ApiAduan::index');



$routes->get('/testapi', 'Aduan::testapicall');


// $routes->post('/laporan', [Test::class,'index2']);

// $routes->view('about', 'test');


