<?php

use App\Controllers\Laporan\Test;
use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
// $routes->get('/', 'Home::index');
// $routes->get('/test/(:segment)/(:segment)', 'Home::index2/$1/$2');
// $routes->get('/test', 'Home::index2');





$routes->group('kpp', static function ($routes) {
    $routes->get('laporan', 'Laporan\Test::index', ['as' => 'laporan1']);
    $routes->post('laporan', [Test::class,'index2']);
    $routes->view('about', 'test');
});


