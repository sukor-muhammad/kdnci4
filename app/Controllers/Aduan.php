<?php

namespace App\Controllers;

use App\Models\ComplainantDetails;
use App\Controllers\BaseController;

class Aduan extends BaseController
{
    public function index()
    {
        $d['statusLookup']=generalLookup('complaint_status');
        return view('aduan/index',$d);
        //
    }

    public function ajaxDataAduan()
    {

        // define column
        $column[1]='complainant_details.complainant_name';
        $column[2]='al.name';
        $column[3]='al2.name';

        // dd($this->request->getVar());

        $draw=$this->request->getVar('draw');
        $length=$this->request->getVar('length');
        $start=$this->request->getVar('start');
        $search=$this->request->getVar('search');
        $order=$this->request->getVar('order');
        $searchdata=$this->request->getVar('searchdata');
        
        

        $model=new ComplainantDetails();
      $dataModel=$model
      ->select('complainant_details.*,al.name as citizen,al2.name as status')
      ->join('asset_lookup as al', 'al.code = complainant_details.complainant_nationality','left')
      ->join('asset_lookup as al2', 'al2.code = complainant_details.complainant_status','left')
    //   ->withDeleted()
      ->asObject();

      $recordsTotal=$dataModel->countAllResults(false);

    //   carian
        $complainant_name=$searchdata['complainant_name']??'';
    if($complainant_name??''){

        $dataModel->like('complainant_details.complainant_name',$complainant_name);

    }

    $complainant_status=$searchdata['complainant_status']??'';
    if($complainant_status??''){

        $dataModel->where('complainant_details.complainant_status',$complainant_status);

    }


// carian default

if($search['value']??''){
    $dataModel->groupStart();
    foreach($column as $rowsrch){

        $dataModel->orlike($rowsrch,$search['value'],'both');

    }
    $dataModel->groupEnd();

}

// end carian default

// sorting
if($order[0]['column']??''){

    $nocolumn=$order[0]['column'];
    $nocolumnorder=$column[$nocolumn];
    $orderby=$order[0]['dir'];

    $dataModel->orderBy($nocolumnorder,$orderby);


}



// end sorting




    $dataModel->limit($length,$start);

    $recordsFiltered=$dataModel->countAllResults(false);
   
   $query= $dataModel->get();
   $bil=0;

// encrypter
$encrypter=\Config\Services::encrypter();


        if($query->getNumRows()>0){
            foreach($query->getResult() as $row){

                

                $id=$row->id;

               $eid= $encrypter->encrypt($id);
               $eid=bin2hex($eid);

               $urldelete=url_to('admin.aduan.delete',$eid);

               $delete=' <form action="'.$urldelete.'" method="POST">
               <input type="hidden" name="'.csrf_token().'" value="'.csrf_hash().'" />
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>';



                $dt['data'][$bil][]=++$start;
                $dt['data'][$bil][]=$row->complainant_name;
                $dt['data'][$bil][]=$row->citizen;
                $dt['data'][$bil][]=$row->status;
                $dt['data'][$bil][]="
                
                <div class='row row-cols-auto'>
                <div class='col'>
                <a href='".url_to('Aduan::edit',$eid).
                "'  class='btn btn-info'>kemaskini</a>
                </div><div class='col'>". $delete."</div></div>
                ";

                $bil++;

            }


        }


      


        $dt['draw']= $draw;
        $dt['recordsTotal']=$recordsTotal??0;
        $dt['recordsFiltered']=$recordsFiltered??0;


        return json_encode($dt);


        
    }



    public function edit($id)
    {
  
       if (ctype_xdigit($id) && strlen($id) % 2 == 0) {
        $encrypter = \Config\Services::encrypter();
        $id = $encrypter->decrypt(hex2bin($id));   
    } else {
        throw new \Exception('error');
    }

   $model=new ComplainantDetails();


     $dataModel=  $model->asObject()->find($id);

  

      $d['statusLookup']=generalLookup('complaint_status');
      $d['dataModel']=$dataModel;
      
      return view('aduan/edit',$d);



        // $d['statusLookup']=generalLookup('complaint_status');
        // return view('aduan/index',$d);
        //
    }


    public function update($id)
    {
        $rule=[
            'complainant_name'=>'required',
            'complainant_status'=>'required',
            'complainant_identity'=>'required'
        ];

        $msgrule=[
            'complainant_identity'=>['required'=>'test'],
            'complainant_status'=>['required'=>'test']
        ];

        if(! $this->validate($rule,$msgrule)){
   
            return redirect()->back()->withInput();

        }

        $validate=$this->validator->getValidated();

        // $validate['complainant_nationality']='CIT001';

        $model= new ComplainantDetails();

        // $dataModel=  $model->find($id);
    
        // $model->save($validate);

        if( $model->update($id,$validate)){

            return redirect()->route('Aduan::index');
        }
        
    //   dd($validate);



    }

    public function delete($id)
    {

        if (ctype_xdigit($id) && strlen($id) % 2 == 0) {
            $encrypter = \Config\Services::encrypter();
            $id = $encrypter->decrypt(hex2bin($id));   
        } else {
            throw new \Exception('error');
        }


        $model= new ComplainantDetails();


        if( $model->delete($id)){

            return redirect()->route('Aduan::index');
        }




    }

    function callApiBantuan(){



$curl = curl_init();

curl_setopt_array($curl, [
  CURLOPT_URL => "http://localhost/kdnci4/public/index.php/api/aduan?pages=5",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => [
    "Authorization: Bearer 9835c0e650da74563fe0b547cf497040eedde0a762e31b4367ce47bd71a67004",
  ],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

  $data=  json_decode($response,true);
  dd($data['data']);
}
    }


    function callApiBantuan2(){

        $page=$this->request->getVar('page')??1;

        $client = \Config\Services::curlrequest();

$response = $client->request('GET', "http://localhost/kdnci4/public/index.php/api/aduan?page=$page", [
    'headers' => ['Authorization'=>'Bearer 9835c0e650da74563fe0b547cf497040eedde0a762e31b4367ce47bd71a67004'],
]);

$body=json_decode($response->getBody());

// public current_page -> integer 26
// ⇄public first_page -> integer 1
// ⇄public from -> integer 251
// ⇄public last_page -> integer 30
// ⇄public per_page -> integer 10
// ⇄public to -> integer 260
// ⇄public total -> integer 300

$d['data']=$body;


$pager = service('pager');

$page    = $body->pagination->current_page;
$perPage = $body->pagination->per_page;
$total   = $body->pagination->total;

// Call makeLinks() to make pagination links.
$pager_links = $pager->makeLinks($page, $perPage, $total,'pager_bootstrap');


    $d['pager_links'] = $pager_links;



return view('aduan/index_api',$d);






    }



}
