<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;

class ApiAduan extends ResourceController
{
    protected $modelName='App\Models\ComplainantDetails';
    protected $format='json';

    protected $filters=['csrf'=>['except'=>['create']]];

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {

        
     $model=$this->model;
      $data=  $this->model
      ->select('complainant_details.complainant_name,
      complainant_details.complainant_nationality,
      complainant_details.complainant_identity')
      ->paginate(10);


      return $this->respond([
        'data' => $data,
        'pagination' => [
            'total' => $model->pager->getTotal(),
            'per_page' => $model->pager->getPerPage(),
            'current_page' => $model->pager->getCurrentPage(),
            'last_page' => $model->pager->getPageCount(),
            'first_page' => 1, // Set to 1
            'last_page' => $model->pager->getPageCount(),
            'from' => ($model->pager->getPerPage() * ($model->pager->getCurrentPage() - 1)) + 1,
            'to' => min($model->pager->getTotal(), $model->pager->getPerPage() * $model->pager->getCurrentPage()),
        ],
    ]);


    //   dd($data);

        //
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        //
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        //
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        $rule=[
            'complainant_name'=>'required',
            'complainant_status'=>'required',
            'complainant_identity'=>'required'
        ];

        // $msgrule=[
        //     'complainant_identity'=>['required'=>'test','alpha'=>'hanya ch '],
        //     'complainant_status'=>['required'=>'test']
        // ];

        if(! $this->validate($rule)){
   
           return $this->failValidationErrors($this->validator->getErrors());

        }

        $validate=$this->validator->getValidated();

        $validate['complainant_nationality']='CIT001';

        $model=$this->model;
    
        // $model->save($validate);

        if( $model->insert($validate)){

           return $this->respondCreated(['status'=>201,
           'messages'=>['seuccess'=>'seccessfuly created']
           
        ]);

        }

        //
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
       

        $model=$this->model->find($id);

        if(is_null($model)){
            return $this->failForbidden('tiada data');
        }

       
        $input=$this->request->getRawInput();

        $data=[
            'id'=>$id,
            'complainant_name'=>$input['complainant_name'],
            'complainant_status'=>$input['complainant_status'],
            'complainant_identity'=>$input['complainant_identity'],
        ];

        $rule=[
            'complainant_name'=>'required',
            'complainant_status'=>'required',
            'complainant_identity'=>'required'
        ];

        $this->model->setValidationRules($rule);



        if( $this->model->save($data)){

                    return $this->respondUpdated(['status'=>201,
                    'messages'=>['seuccess'=>'seccessfuly update']
                    
                ]);

            }else{
                return $this->failValidationErrors($this->model->errors());
            }


        
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $model=$this->model->find($id);

        if(is_null($model)){
            return $this->failForbidden('tiada data');
        }

       
        if($this->model->delete($id)){

            return $this->respondDeleted(['status'=>201,
            'messages'=>['seuccess'=>'seccessfuly Delete']
            
        ]);

        }else{
            return $this->failForbidden('tidak Berjaya');
        }



        //
    }
}
