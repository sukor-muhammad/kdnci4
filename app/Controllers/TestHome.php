<?php

namespace App\Controllers;

use App\Models\AssetLookup;
use App\Models\ComplainantDetails;
use App\Controllers\BaseController;

class TestHome extends BaseController
{

   
    

    public function create()
    {
        $rule=[
            'complainant_name'=>'required',
            'complainant_status'=>'required',
            'complainant_identity'=>'required|alpha'
        ];

        $msgrule=[
            'complainant_identity'=>['required'=>'test','alpha'=>'hanya ch '],
            'complainant_status'=>['required'=>'test']
        ];

        if(! $this->validate($rule,$msgrule)){
   
            return redirect()->back()->withInput();

        }

        $validate=$this->validator->getValidated();

        $validate['complainant_nationality']='CIT001';

        $model= new ComplainantDetails();
    
        // $model->save($validate);

        if( $model->insert($validate)){

            return redirect()->route('TestHome::index');
        }
        
    //   dd($validate);



    }

    public function new()
    {
        $d['statusLookup']=generalLookup('complaint_status');
      
        return view('test_home/new',$d);
    }

    public function index()
    {

     




      $model=new ComplainantDetails();
      $dataModel=$model
      ->select('complainant_details.*,al.name as citizen,al2.name as status')
      ->join('asset_lookup as al', 'al.code = complainant_details.complainant_nationality','left')
      ->join('asset_lookup as al2', 'al2.code = complainant_details.complainant_status','left')
      ->asObject();

    //   carian
        $complainant_name=$this->request->getVar('complainant_name');
    if($complainant_name??''){

        $dataModel->like('complainant_details.complainant_name',$complainant_name);

    }

    $complainant_status=$this->request->getVar('complainant_status');
    if($complainant_status??''){

        $dataModel->where('complainant_details.complainant_status',$complainant_status);

    }

    // end carian
      
      $dataModel1= $dataModel->paginate(10,'std');
    //   ->findAll();

    //   echo '<pre>';
    //     dd($dataModel1);
    //   echo '</pre>';
    //   die();

      $d['model']=$dataModel1;
      $d['pager']=$model->pager;

      $datapager= countFrom($model->pager);

      $d['show']= $datapager['show'];
      $d['count']= $datapager['count'];

  
        // $d['statusLookup']=statusLookup();
        $d['statusLookup']=generalLookup('complaint_status');
     


    

        // return view('layouts/main',$d);
        return view('test_home/index',$d);
        //
    }


}
