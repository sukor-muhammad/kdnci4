<div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <label for="first-name-horizontal">Name</label>
            </div>
            <div class="col-md-8 form-group">
                <input type="text" id="first-name-horizontal" class="form-control" name="complainant_name" placeholder="Name">
            </div>
            <div class="col-md-4">
                <label for="email-horizontal">Status</label>
            </div>
            <div class="col-md-8 form-group">
                <select name='complainant_status' class="form-select" id="basicSelect">
                    <option value=''>Sila Piliah</option>
                    <?php
                    foreach ($statusLookup as $key => $value) {

                    ?>
                        <option value='<?= $key ?>'><?= $value ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <?php

            ?>
            <div class="col-md-4">
                <label for="contact-info-horizontal">Mykad</label>
            </div>
            <div class="col-md-8 form-group">
                <input value='<?=old('complainant_identity')?>' 
                type="text" id="contact-info-horizontal" 
                class="form-control <?=validation_show_error('complainant_identity')?'is-invalid':''  ?> "
                 name="complainant_identity" placeholder="mykad">
                <div class="invalid-feedback">
                <?=validation_show_error('complainant_identity')?>
                </div>
            </div>

