<?= $this->extend('layouts/main') ?>

<?= $this->section('page_title') ?>
ADUAN
<?= $this->endSection() ?>



<?= $this->section('content') ?>

<form class="form form-horizontal">
    <div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <label for="first-name-horizontal">Name</label>
            </div>
            <div class="col-md-8 form-group">
                <input type="text" id="first-name-horizontal" class="form-control" name="complainant_name" placeholder="Name">
            </div>
            <div class="col-md-4">
                <label for="email-horizontal">Status</label>
            </div>
            <div class="col-md-8 form-group">
                <select name='complainant_status' class="form-select" id="basicSelect">
                    <option value=''>Sila Piliah</option>
                    <?php
                    foreach ($statusLookup as $key => $value) {

                    ?>
                        <option value='<?= $key ?>'><?= $value ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-4">
                <label for="contact-info-horizontal">Mobile</label>
            </div>
            <div class="col-md-8 form-group">
                <input type="number" id="contact-info-horizontal" class="form-control" name="contact" placeholder="Mobile">
            </div>



            <div class="col-sm-12 d-flex justify-content-end">
                <button type="submit" class="btn btn-primary me-1 mb-1">Cari</button>

                <a href='<?= url_to('TestHome::index'); ?>' class="btn btn-light-secondary me-1 mb-1">Reset</a>
            </div>
        </div>
    </div>
</form>

<table id="aduanTable" class="table" style="width:100%">
    <thead>
        <tr>
            <th>Bil</th>
            <th>Name</th>
            <th>Citizen</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Bil</th>
            <th>Name</th>
            <th>Citizen</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>

<?= $this->endSection() ?>


<?= $this->section('content_script') ?>
<script>
    $(document).ready(function() {

        new DataTable('#aduanTable', {
            dom: '<"float-end mb-2"B><"d-flex justify-content-between"l><"d-flex justify-content-end"f>t<"d-flex justify-content-between"i><"float-end"p>',

buttons: [
     'csv', 'excel', 'pdf',
],
    "fnServerParams": function (aoData) {
     aoData['searchdata'] = <?=json_encode($_GET)?>;
},


            ajax: '<?=url_to('admin.aduan.ajaxDataAduan')?>',
            processing: true,
            serverSide: true
        });


    });
</script>

<?= $this->endSection() ?>