<?= $this->extend('layouts/main') ?>

<?= $this->section('page_title') ?>
ADUAN
<?= $this->endSection() ?>



<?= $this->section('content') ?>

<table class='table'>
    <tr>
        <th>Bil</th>
        <th>Name</th>
        <th>Citizen</th>
        <th>Status</th>
    </tr>


    <?php
    // start foreach model
    // dd($data->pagination);

    $from=$data->pagination->from;
    $last_pageori=$last_page=$data->pagination->last_page;
    $current_page=$last_page=$data->pagination->current_page;
    if($current_page > 1){
        $Previous=$current_page-1;
    }
    $Next=$current_page-1;
    

    foreach ($data->data as $row) {
    ?>



        <tr>
        <td><?=$from++?></td>
            <!-- <td><? //=$row['complainant_name']
                        ?></td> -->

            <td><?= $row->complainant_name ?></td>
            <td><?= $row->complainant_nationality ?></td>
            <td><?= $row->complainant_identity ?></td>
        </tr>

    <?php
    } //end foreach model
    ?>


</table>

<?=$pager_links?>

<nav aria-label="Page navigation example">
  <ul class="pagination">
  <li class="page-item">
      <a class="page-link" href="http://localhost/kdnci4/public/index.php/admin/aduanapi2?page=1" aria-label="first">
        <span aria-hidden="true">First</span>
      </a>
    </li>
    <li class="page-item">
      <a class="page-link" href="http://localhost/kdnci4/public/index.php/admin/aduanapi2?page=<?=$Previous?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <?php
    for ($i=$current_page; $i <=$current_page+5 ; $i++) { 
        # code...
    
    ?>
    <li class="page-item"><a class="page-link" href="http://localhost/kdnci4/public/index.php/admin/aduanapi2?page=<?=$i?>"> <?=$i?></a></li>

    <?php
    }
    ?>
    <li class="page-item">
      <a class="page-link" href="http://localhost/kdnci4/public/index.php/admin/aduanapi2?page=<?=$Next?>" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
    <li class="page-item">
      <a class="page-link" href="http://localhost/kdnci4/public/index.php/admin/aduanapi2?page=<?=$last_pageori?>>" aria-label="Last">
        <span aria-hidden="true">last</span>
      </a>
    </li>
  </ul>
</nav>

<?= $this->endSection() ?>