<?= $this->extend('layouts/main') ?>

<?= $this->section('page_title') ?>
titel 1
<?= $this->endSection() ?>



<?= $this->section('content') ?>

<!-- form serch -->
<!-- <form action="">
<input type="text" >

</form> -->

<form class="form form-horizontal">
    <div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <label for="first-name-horizontal">Name</label>
            </div>
            <div class="col-md-8 form-group">
                <input type="text" id="first-name-horizontal" class="form-control" name="complainant_name" placeholder="Name">
            </div>
            <div class="col-md-4">
                <label for="email-horizontal">Status</label>
            </div>
            <div class="col-md-8 form-group">
                <select name='complainant_status' class="form-select" id="basicSelect">
                    <option value=''>Sila Piliah</option>
                    <?php
                    foreach ($statusLookup as $key => $value) {

                    ?>
                        <option value='<?= $key ?>'><?= $value ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-4">
                <label for="contact-info-horizontal">Mobile</label>
            </div>
            <div class="col-md-8 form-group">
                <input type="number" id="contact-info-horizontal" class="form-control" name="contact" placeholder="Mobile">
            </div>



            <div class="col-sm-12 d-flex justify-content-end">
                <button type="submit" class="btn btn-primary me-1 mb-1">Cari</button>

                <a href='<?= url_to('TestHome::index'); ?>' class="btn btn-light-secondary me-1 mb-1">Reset</a>
            </div>
        </div>
    </div>
</form>

<a href='<?= url_to('test.new'); ?>' class="btn btn-info me-1 mb-1">Tambah</a>
<!-- end for serch -->
<table class='table'>
    <tr>
        <th>Bil</th>
        <th>Name</th>
        <th>Citizen</th>
        <th>Status</th>
    </tr>


    <?php
    // start foreach model
    foreach ($model as $row) {
    ?>


        <tr>
        <td><?=++$count?></td>
            <!-- <td><? //=$row['complainant_name']
                        ?></td> -->

            <td><?= $row->complainant_name ?></td>
            <td><?= $row->citizen ?></td>
            <td><?= $row->status ?></td>
        </tr>

    <?php
    } //end foreach model
    ?>


</table>
<?=$show?>
<div class="pagination pagination-primary  justify-content-end">
    <?= $pager->links('std', 'pager_bootstrap') ?>
</div>



<?= $this->endSection() ?>