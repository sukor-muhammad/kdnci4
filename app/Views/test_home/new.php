<?= $this->extend('layouts/main') ?>

<?= $this->section('page_title') ?>
new 
<?= $this->endSection() ?>



<?= $this->section('content') ?>

<?php
print_r(validation_errors());
?>

<?=form_open(url_to('test.create'),['class'=>'form form-horizontal'])?>
<!-- <form class="form form-horizontal"> -->
    <div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <label for="first-name-horizontal">Name</label>
            </div>
            <div class="col-md-8 form-group">
                <input type="text" id="first-name-horizontal" class="form-control" name="complainant_name" placeholder="Name">
            </div>
            <div class="col-md-4">
                <label for="email-horizontal">Status</label>
            </div>
            <div class="col-md-8 form-group">
                <select name='complainant_status' class="form-select" id="basicSelect">
                    <option value=''>Sila Piliah</option>
                    <?php
                    foreach ($statusLookup as $key => $value) {

                    ?>
                        <option value='<?= $key ?>'><?= $value ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <?php

            ?>
            <div class="col-md-4">
                <label for="contact-info-horizontal">Mykad</label>
            </div>
            <div class="col-md-8 form-group">
                <input value='<?=old('complainant_identity')?>' 
                type="text" id="contact-info-horizontal" 
                class="form-control <?=validation_show_error('complainant_identity')?'is-invalid':''  ?> "
                 name="complainant_identity" placeholder="mykad">
                <div class="invalid-feedback">
                <?=validation_show_error('complainant_identity')?>
                </div>
            </div>



            <div class="col-sm-12 d-flex justify-content-end">
                <button type="submit" class="btn btn-primary me-1 mb-1">Cari</button>

                <a href='<?= url_to('TestHome::index'); ?>' class="btn btn-light-secondary me-1 mb-1">Reset</a>
            </div>
        </div>
    </div>
</form>


<?= $this->endSection() ?>